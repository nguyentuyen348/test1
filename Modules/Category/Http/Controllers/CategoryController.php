<?php

namespace Modules\Category\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Intervention\Image\Facades\Image;
use Modules\Category\Entities\Category;
use PHPUnit\Exception;

class CategoryController extends Controller
{
    public function store(Category $category,Request $request)
    {
        try {
            if($request->get('image'))
            {
                $image = $request->get('image');
                $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
                Image::make($request->get('image'))->save(public_path('storage/images/').$name);
                $url_name='/storage/images/'.$name;
            }
            $category->image=$url_name;
            $category->name=$request->name;
            $category->save();
            return response()->json([
                'status'=>'create success'
            ]);
        }catch (Exception $exception){
            return response()->json($exception->getMessage());
        }
    }

    public function index()
    {
        $categories = Category::all();
        return response()->json($categories);
    }

    public function edit($id){
        $category = Category::find($id);
        return response()->json($category);
    }

    public function update($id, Request $request){
        $category = Category::find($id);
        try {
            if($request->get('image'))
            {
                $image = $request->get('image');
                $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
                Image::make($request->get('image'))->save(public_path('storage/images/').$name);
            }
            $category->image=$name;
            $category->name=$request->name;
            $category->save();
            return response()->json([
                'status'=>'create success'
            ]);
        }catch (Exception $exception){
            return response()->json($exception->getMessage());
        }
    }
    public function delete($id){
        $category = Category::find($id);
        $category->delete();
        return response()->json([
            'status' => 'delete success'
        ]);
    }
}
