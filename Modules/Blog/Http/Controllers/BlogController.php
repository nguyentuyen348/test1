<?php

namespace Modules\Blog\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Blog\Entities\Post;

class BlogController extends Controller
{
    public function store(Post $post, Request $request)
    {
        try {
            $post->user_id=$request->user_id=1;
            $post->title=$request->title;
            $post->content_post=$request->content_post;
            $post->save();
            return response()->json([
                'status'=>'create success'
            ]);
        }catch (\Exception $exception){
            return response()->json($exception->getMessage());
        }
    }

    public function index()
    {
        $posts=Post::all();
        return response()->json($posts);
    }

    public function detail($id)
    {
        $post=Post::find($id);
        return response()->json($post);
    }


    public function edit($id)
    {
        $post=Post::find($id);
        return response()->json($post);
    }

    public function update($id, Request $request)
    {
        $post=Post::find($id);
        try {
            $post->user_id=$request->user_id=1;
            $post->title=$request->title;
            $post->content_post=$request->content_post;
            $post->save();
            return response()->json([
                'status'=>'update success'
            ]);
        }catch (\Exception $exception){
            return response()->json($exception->getMessage());
        }
    }
    public function delete($id)
    {
        $post = Post::find($id);
        $post->delete();
        return response()->json('successfully deleted');
    }
}
