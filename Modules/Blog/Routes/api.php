<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\Blog\Http\Controllers\BlogController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/blog', function (Request $request) {
    return $request->user();
});

Route::prefix('post')->group(function (){
   Route::post('create', "BlogController@store");
   Route::get('index', "BlogController@index");
   Route::get('edit/{id}',"BlogController@edit");
   Route::post('update/{id}',"BlogController@update");
   Route::delete('delete/{id}', "BlogController@delete");
   Route::get('detail/{id}',"BlogController@detail");
});
