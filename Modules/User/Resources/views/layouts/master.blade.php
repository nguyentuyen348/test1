<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Module User</title>
        <link rel="stylesheet" href="master.css">
       {{-- Laravel Mix - CSS File --}}
       {{-- <link rel="stylesheet" href="{{ mix('css/user.css') }}"> --}}

    </head>
    <body>
    <div>
        <div class="container">
            <div class="header">
                <div class="frame">
                    <img src="/storage/images/Frame.png" alt="resources/js/src/assets/Frame.png">
                </div>
            </div>
            <div class="rectangle">
                <hr class="rec">
            </div>

        @yield('content')

            <div id="footer">
            </div>
        </div>
    </div>
        {{-- Laravel Mix - JS File --}}
        {{-- <script src="{{ mix('js/user.js') }}"></script> --}}
    </body>
</html>
