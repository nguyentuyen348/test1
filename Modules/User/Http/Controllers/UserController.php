<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Modules\Product\Entities\Product;
use Modules\User\Entities\User;

class UserController extends Controller
{
    public function index()
    {
        return view('user::index');
    }

    public function profile($id)
    {
        $user=User::find($id);
        return response()->json($user);
    }

    public function register(Request $request)
    {
        DB::beginTransaction();
        try {
            $user = new \App\Models\User();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->save();
            DB::commit();
            $data = [
                'status' => 'success',
                'user' => $user,
                'message' => 'Register success!'
            ];
            return response()->json($data);
        } catch (Exception $exception) {
            DB::rollBack();
            $data = [
                'status' => 'error',
                'message' => $exception->getMessage()
            ];
            return response()->json($data);
        }
    }

    public function login(Request $request)
    {
        $email=$request->email;
        $password=$request->password;

        $data= [
            'email'=>$email,
            'password'=>$password
        ];

        if (Auth::attempt($data)){
            return response()->json([
                'email'=>$email,
                'status'=>'success',
                'message'=>'Login success',
            ]);
        }
        else {
            return response()->json([
                'status' => 'error',
                'message' => 'Your email address or password is incorrect. Please try again',
            ]);
        }
    }

    public function logout()
    {
        try {
            auth()->logout();
            return response()->json([
                'status'=>'success',
                'message'=>'Logout success !'
            ]);
        }
        catch (Exception $exception){
            return response()->json([
                'status'=>'error',
                'message'=>'Error !'
            ]);
        }
    }
}
