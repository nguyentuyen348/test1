import Vue from 'vue'
import Router from 'vue-router'
import Login from "../../../../Modules/User/Resources/js/src/components/Login";
import Create from "../../../../Modules/User/Resources/js/src/components/Create";
import BlogCreate from "../../../../Modules/Blog/Resources/js/src/components/BlogCreate";
import BlogIndex from "../../../../Modules/Blog/Resources/js/src/components/BlogIndex";
import BlogEdit from "../../../../Modules/Blog/Resources/js/src/components/BlogEdit";
window.Vue = require('vue');

import VueRouter from 'vue-router';
Vue.use(VueRouter);

import VueAxios from 'vue-axios';
import axios from 'axios';
import BlogDetail from "../../../../Modules/Blog/Resources/js/src/components/BlogDetail";
import CategoryIndex from "../../../../Modules/Category/Resources/js/src/components/CategoryIndex";
import CategoryCreate from "../../../../Modules/Category/Resources/js/src/components/CategoryCreate";
import CategoryEdit from "../../../../Modules/Category/Resources/js/src/components/CategoryEdit";

Vue.use(VueAxios, axios);

Vue.use(Router)


export default new Router({
    routes: [
        {
            path: '/login',
            name: 'Login',
            component: Login
        },
        {
            path: '/create',
            name: 'Create',
            component: Create
        },
        {
            path: '/blog/create',
            name: 'BlogCreate',
            component: BlogCreate
        },
        {
            path: '/blog/edit/:id',
            name: 'BlogEdit',
            component: BlogEdit
        },
        {
            path: '/blog/index',
            name: 'BlogIndex',
            component: BlogIndex
        },
        {
            path: '/blog/detail/:id',
            name: 'BlogDetail',
            component: BlogDetail
        },
        {
            path: '/category/index',
            name: 'CategoryIndex',
            component: CategoryIndex
        },
        {
            path: '/category/create',
            name: 'CategoryCreate',
            component: CategoryCreate
        },
        {
            path: '/category/edit/:id',
            name: 'CategoryEdit',
            component: CategoryEdit
        }
    ]
})

